


# Building

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])
With python custom builder
==========================
```
zxvcv.conan-toolbox build $(id -u ${USER}) $(id -g ${USER}) --upload-strategy NEVER --log debug
```

Gitlab require to login once again after a while of absence when uploading: ```conan user <username> -r gitlab -p```

Step by step
============
```
TODO[PP]: ...
```

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])


# Testing

[comment]: # ([[_DOCMD_USER_BLOCK_1_BEGIN]])
Package is a header only library - so it does not contain any code to test.

[comment]: # ([[_DOCMD_USER_BLOCK_1_END]])


# Generating Documentation

[comment]: # ([[_DOCMD_USER_BLOCK_2_BEGIN]])
```
python3 -m docmd generate include/errors.h --log debug
```

[comment]: # ([[_DOCMD_USER_BLOCK_2_END]])

[comment]: # ([[_DOCMD_USER_BLOCK_3_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_3_END]])

***
[Main page](../README.md) | [Repository url](https://gitlab.com/zxvcv-conan/errors) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>
