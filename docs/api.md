


# API

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])

</br>



## Enums
***

[comment]: # ([[_DOCMD_USER_BLOCK_3_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_3_END]])

> ## Std_Err_Tag
> ***
> Error code enumerator.
>
> Contains easy to use list of exit codes from functions etc.<br>Allow to standarize outputs and crate easyli to read, clear code.
>
> ### <u><i>Values:</i></u>
> |NAME|VALUE|DESCRIPTION|
> |-|-|-|
> |STD_OK|0|Execution ends successfully.|
> |STD_ERROR|1| The most vague error code.<br> Should be used if no of ohter error codes could be used. |
> |STD_ALLOC_ERROR|2|Error during memory allocation.|
> |STD_REFERENCE_ERROR|3|Error during accessing values or pointers.|
> |STD_PARAMETER_ERROR|4|Wrong parameter or other problem with parameter.|
> |STD_BUSY_ERROR|5| |
> |STD_TIMEOUT_ERROR|6| |
> |STD_INTERRUPTED_ERROR|7|Execution of the code was interrupted.|
> |STD_IO_ERROR|8|Problem with intput output interface.|
> |STD_COMMAND_ERROR|9| |
> |STD_VALUE_ERROR|10|Vrong value error.|
> |STD_SIZE_OVERFLOW_ERROR|11|Overflow of variable occured.|
> |STD_NO_VALUE_ERROR|12|There is no value associated.|
> |STD_TERMINATION_ERROR|13|Execution was terminated.|
> |STD_EXECUTION_ERROR|14| |
> |STD_INITIALIZATION_ERROR|15|Problem with value or component initialization.|
> |STD_NOOPERATION_ERROR|16| |
> |STD_RUNTIME_ERROR|17|Execution problem.|

</br>



## Typedefs
***

[comment]: # ([[_DOCMD_USER_BLOCK_4_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_4_END]])

> ## Std_Err
> ***
> Error code type.
>
> ### <u><i>Definition:</i></u>
> Std_Err_Tag
>
> ### <u><i>Description:</i></u>
> This enumerator represents all exit codes that could been returned from components.
>

</br>



</br>


***
[Main page](../README.md) | [Repository url](https://gitlab.com/zxvcv-conan/errors) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>
