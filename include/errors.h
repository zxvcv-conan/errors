// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================


#ifndef ZXVCV_ERRORS_H_
#define ZXVCV_ERRORS_H_


// =================================== INCLUDES =======================================


// ================================== DATA TYPES ======================================

/** Error code type.
 *
 * This enumerator represents all exit codes that could been returned from components.
 */
typedef
/** Error code enumerator.
 *
 * Contains easy to use list of exit codes from functions etc.<br>
 * Allow to standarize outputs and crate easyli to read, clear code.
 */
enum Std_Err_Tag{
    /** Execution ends successfully. */
    STD_OK                      = 0,

    /**
     * The most vague error code.<br>
     * Should be used if no of ohter error codes could be used.
     */
    STD_ERROR                   = 1,

    /** Error during memory allocation. */
    STD_ALLOC_ERROR             = 2,

    /** Error during accessing values or pointers. */
    STD_REFERENCE_ERROR         = 3,

    /** Wrong parameter or other problem with parameter. */
    STD_PARAMETER_ERROR         = 4,

    STD_BUSY_ERROR              = 5,
    STD_TIMEOUT_ERROR           = 6,

    /** Execution of the code was interrupted. */
    STD_INTERRUPTED_ERROR       = 7,

    /** Problem with intput output interface. */
    STD_IO_ERROR                = 8,

    STD_COMMAND_ERROR           = 9,

    /** Vrong value error. */
    STD_VALUE_ERROR             = 10,

    /** Overflow of variable occured. */
    STD_SIZE_OVERFLOW_ERROR     = 11,

    /** There is no value associated. */
    STD_NO_VALUE_ERROR          = 12,

    /** Execution was terminated. */
    STD_TERMINATION_ERROR       = 13,

    STD_EXECUTION_ERROR         = 14,

    /** Problem with value or component initialization. */
    STD_INITIALIZATION_ERROR    = 15,

    STD_NOOPERATION_ERROR       = 16,

    /** Execution problem. */
    STD_RUNTIME_ERROR           = 17
} Std_Err;


// ============================== PUBLIC DECLARATIONS =================================


#endif // ZXVCV_FIFO_H_
