from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout


class ConanPackage(ConanFile):
    name = "errors"
    version = "0.1.5"

    # Optional metadata
    license = "Eclipse Public License - v 2.0"
    author = "Pawel Piskorz ppiskorz0@gmail.com"
    url = "https://gitlab.com/zxvcv-conan/errors"
    description = """Error codes.
    Implementation for return error codes and error handling.
    """
    topics = ("errors", "C", "embedded", "interrupts")

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "include/*"
    no_copy_source = True

    def package(self):
        self.copy("*.h")
