cmake_minimum_required(VERSION 3.15)
project(errors C)

#add_library(errors src/errors.c)
target_include_directories(errors PUBLIC include)

set_target_properties(errors PROPERTIES PUBLIC_HEADER "include/errors.h")
install(TARGETS errors)
