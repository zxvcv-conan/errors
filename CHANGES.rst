Changelog
=========

0.1.5 (2023-02-19)
------------------
- Change CRLF to LF in all files.
- Add .vscode/settings.json file.

0.1.4 (2022-11-02)
------------------
- Documentation generated.
- Rewrite file errors.h.

0.1.3 (2022-10-15)
------------------
- New error code STD_INITIALIZATION_ERROR.
- New error code STD_NOOPERATION_ERROR.
- New error code STD_RUNTIME_ERROR.

0.1.2 (2022-10-15)
------------------
- New error code STD_TERMINATION_ERROR.
- New error code STD_EXECUTION_ERROR.

0.1.1 (2022-10-12)
------------------
- New error code STD_VALUE_ERROR.
- New error code STD_SIZE_OVERFLOW_ERROR.
- New error code STD_NO_VALUE_ERROR.

0.1.0 (2022-09-27)
------------------
- Build system restructurization.
- Conan build settings updates.

0.0.1 (2022-07-04)
------------------
- Initial commit.
- Basic implementation.
